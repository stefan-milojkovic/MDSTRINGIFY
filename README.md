# MDSTRINGIFY
**MDSTRINGIFY** is a Python based software package for the analysis of string-like cooperative atomic motion in molecular dynamics (MD) simulations. The codebase along with the full README will be made available in this GitLab repository under the MIT license once the software is ready for public usage.
<br>
<br>
Members of the **University of Twente** can access the internal project [here](https://gitlab.utwente.nl/stefan-milojkovic/mdstringify-internal).